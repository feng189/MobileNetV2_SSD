import tensorflow as tf
import numpy as np
import os
import PIL.Image as Image
import json


def create_dataset(base_dir):
    CATE_GROY ={'hand_small': 1, 'hand_large':2}
    name_list = [os.path.splitext(i)[0] for i in os.listdir(os.path.join(base_dir,'image')) if i.endswith('.jpg')]
    def gen():
        for item in name_list:
            img_filename = os.path.join(base_dir, 'image', item+'.jpg')
            label_filename = os.path.join(base_dir, 'label', item+'.json')
            assert os.path.exists(img_filename)
            assert os.path.exists(label_filename)
            image = np.array(Image.open(img_filename).convert('RGB'))

            gt_boxes = []
            gt_category = []
            with open(label_filename, 'r') as f:
                json_file = json.load(f)
                objects = json_file['outputs']['object']
                for i in objects:
                    if i['name'] == 'hand_small' or i['name'] == 'hand_large':
                        gt_boxes.append([i['bndbox']['xmin'], i['bndbox']['ymin'], i['bndbox']['xmax'], i['bndbox']['ymax']])
                        gt_category.append([CATE_GROY[i['name']]])
            yield image, np.array(gt_boxes), np.array(gt_category).squeeze(1)
    
    # gen()
    # dataset = None
    dataset = tf.data.Dataset.from_generator(generator=gen, output_types=(tf.float32, tf.float32, tf.int32))
    return dataset, len(name_list)


if __name__ == '__main__':
    train_dataset, dataset_len = create_dataset(base_dir='/media/wei/Memory/ssd-dataset/train_dataset')
    for i, sample in enumerate(train_dataset,1):
        print(f'#{i} image:{sample[0].shape} gt_boxes:{sample[1].shape} gt_categroy:{sample[2].shape}')