# SSD_MobileNetV2

---

# Requirements
- tensorflow >= 2.0.0
- anaconda, python >=3.7
- Ubuntu 18.04 LTS

---

# Prepare Datasets
Refer ./datasets and ./utils/dataset.py

# Train Model
Refer ./trainer.py

# Convert TFLite Model
Refer ./export_tflite.py

