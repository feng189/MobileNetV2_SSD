import json
import os


if __name__ == '__main__':
    folder = '/media/wei/Memory/ssd-dataset/train_dataset/label'
    name_list = [i for i in os.listdir(folder) if i.endswith('.json')]
    for name in name_list:
        json_name = os.path.join(folder, name)
        assert os.path.exists(json_name)
        with open(json_name, 'r') as f:
            label = f.read()
            label = label.replace('手指', 'figure')
            label = label.replace('小手', 'hand_small')
            label = label.replace('大手', 'hand_large')
            os.remove(json_name)
            with open(json_name, 'w') as f2:
                f2.write(label)